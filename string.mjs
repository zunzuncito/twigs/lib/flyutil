
const default_charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+";

export default class StringUtil {
  static random(length, charset) {
    if (!charset) charset = default_charset;
    let rstr = '';
    for (let i = 0; i < length; i++) {
      const random_index = Math.floor(Math.random() * charset.length);
      rstr += charset[random_index];
    }
    return rstr;
  }

}
