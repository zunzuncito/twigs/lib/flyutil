

import { spawn, execSync } from 'child_process'

export default class Exec {
  static command(cmd, cwd, stdio_inherit) {
    try {
      console.debug(`\x1b[1m\x1b[44m> Execute command: \x1b[0m\x1b[1m ${cmd}`);
      if (!cwd) cwd = process.cwd()
      console.debug(`\x1b[1m\x1b[45m> CWD: \x1b[0m\x1b[1m ${cwd}\x1b[0m`);
      const result = execSync(cmd, {
        cwd: cwd,
        stdio: stdio_inherit ? 'inherit' : undefined
      });
      if (!stdio_inherit) {
        return result.toString();
      } else {
        return undefined;
      }
    } catch (e) {
      throw e;
    }
  }

  static async spawn(cmd, cwd, stdio_inherit) {
    try {
      console.debug(`\x1b[1m\x1b[44m> Spawn command: \x1b[0m\x1b[1m ${cmd}`);
      if (!cwd) cwd = process.cwd()
      console.debug(`\x1b[1m\x1b[45m> CWD: \x1b[0m\x1b[1m ${cwd}\x1b[0m`);
      return spawn(cmd, {
          cwd: cwd,
          stdio: 'inherit',
          shell: true
      });
    } catch (e) {
      throw e;
    }
  }
}
