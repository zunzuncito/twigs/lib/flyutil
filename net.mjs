
const RESERVED_BLOCKS = [
  0, 10, 127, [224, 239], [240, 255]
];


export default class NetUtil {
  static ipv4_reserved(ip_addr) {
    const block = parseInt(ip_addr.split('.')[0]);
    for (let reserved of RESERVED_BLOCKS) {
      if (
        Array.isArray(reserved) &&
        reserved[0] <= block &&
        reserved[1] >= block
      ) {
        return true;
      } else if (reserved == block) {
        return true;
      }
    }
  }

  static ipv4_nuipid(ip_addr, group_size) {
    if (!group_size) group_size = 256;
    const octets = ip_addr.split('.').map(octet => parseInt(octet)).reverse();

    octets[0] = (octets[0] + octets[3] * 4) % 256;
    octets[1] = (octets[1] + octets[3] * 3) % 256;
    octets[2] = (octets[2] + octets[3] * 2) % 256;

    let nuipid = 0;
    for (let i = 0; i < octets.length; i++) {
      nuipid += octets[i] * (256 ** (3 - i));
    }
    return Math.floor(nuipid/group_size);
  }

  static nuipid_ipv4s(nuipid, group_size) {
    if (!group_size) group_size = 256;
    const ips = [];
    const start_index = nuipid * group_size;
    for (let a = start_index; a < start_index+group_size; a++) {
      const octets = [0, 0, 0, 0];
      let current_index = a;
      for (let o = 0; o < octets.length; o++) {
        octets[o] = Math.floor(current_index / (256 ** (3 - o)));
        current_index -= octets[o] * (256 ** (3 - o))
      }

      octets[0] = ((octets[0] - octets[3] * 4) % 256 + 256) % 256;
      octets[1] = ((octets[1] - octets[3] * 3) % 256 + 256) % 256;
      octets[2] = ((octets[2] - octets[3] * 2) % 256 + 256) % 256;

      ips.push(octets.reverse().join("."));
    }
    return ips;
  }
/*
  TODO
  static ipv6_nuipid(ip_addr, group_size) {
    if (!group_size) group_size = 256;
    const segments = ip_addr.split(':');
    const class_indices = segments.map(block => parseInt(block, 16) % group_size);
    const ip_class = 0;
    for (let i = 0; i < 8; i++) {
      ip_class += class_indices[i] * (group_size ** (7 - i));
    }
    return ip_class;
  }
*/
}
