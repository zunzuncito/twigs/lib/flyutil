
import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const fail = FlyTest.Test.fail;

const Assert = FlyTest.Assert;


import NetUtil from '../../../net.mjs'


export default class NUIPID {

  static conflicts() {
    const nuipid_groups = [];

    for (let o1 = 0; o1 < 256; o1++) {
      console.log(` > Block: ${o1}.x.x.x`);
      for (let o2 = 0; o2 < 256; o2 += 32) {
        for (let o3 = 0; o3 < 256; o3 += 32) {
          for (let o4 = 0; o4 < 256; o4 += 32) {
            const test_addr = `${o1}.${o2}.${o3}.${o4}`;

            const nuipid = NetUtil.ipv4_nuipid(test_addr);
            if (!nuipid_groups.includes(nuipid)) {
              const addrs = NetUtil.nuipid_ipv4s(nuipid);
              Assert.equal(addrs.length, 256, `Invalid address list length for NUIPID ${nuipid}!`)
              Assert.true(addrs.includes(test_addr), `${test_addr} has NUIPID ${nuipid}, but it's address list does not include ${test_addr}!`);
            } else {
              nuipid_groups.push(nuipid);
            }
          }
        }
      }
    }

  }

}
