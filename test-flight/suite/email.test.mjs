import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const fail = FlyTest.Test.fail;

const Assert = FlyTest.Assert;

import EmailUtil from '../../email.mjs'

const valid_emails = [
  'a@bc.de', 
  'someemail@domain.tld'
]

const invalid_emails = [
  'invalid@email@domain.com',
  '<script></script>',
  'abc',
  '@something.com',
  'sth@.',
  '@.',
  'a@b.c'
]

export default class EmailUtilTest {
  static validate() {
    test('Validate emails', () => { 
      for (let valid of valid_emails) {
        Assert.true(
          EmailUtil.validate(valid),
          `Email '${valid}' is valid, but validation failed`
        );
      }
      for (let invalid of invalid_emails) {
        Assert.false(
          EmailUtil.validate(invalid),
          `Email '${invalid}' is invalid, but validation passed`
        );
      }
    });
  }

}
