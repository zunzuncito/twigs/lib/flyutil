import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const fail = FlyTest.Test.fail;

const Assert = FlyTest.Assert;


import ArrayUtil from '../../array.mjs'

const array_a = [
  1, "two", { text: "three" }, { duplicate: true}
]

const array_b = [
  3, "four", { text: "five" }, { duplicate: true}
]

const added_ab = JSON.stringify([
  1, "two", { text: "three" }, { duplicate: true},
  3, "four", { text: "five" }
])

export default class ArrayUtilTest {

  static add() {
      test('Add arrays', () => {
        const result = JSON.stringify(ArrayUtil.add(array_a, array_b));
        Assert.equal(result, added_ab, 
          `Unexpected array addition result! Expected: ${added_ab}, got: ${result}`
        );
      });
  }

}
