import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const fail = FlyTest.Test.fail;

const Assert = FlyTest.Assert;

import ObjectUtil from '../../object.mjs'

const object_a = {
  text: "Object string property A",
  obj: {
    sub: {
      sub_arr: [
        "one", "two"
      ]
    },
    arr: [ "a", "b", "c" ]
  },
  arr: [
    { text: "abc" }
  ]
}

const object_b = {
  text: "Object string property B",
  obj: {
    sub: {
      sub_arr: [
        "three"
      ]
    },
    arr: [ "d" ]
  },
  arr: [
    { text: "def" }
  ]
}

const added_ab = JSON.stringify({
  text: "Object string property A",
  obj: {
    sub: {
      sub_arr: [
        "one", "two", "three"
      ]
    },
    arr: [ "a", "b", "c", "d" ]
  },
  arr: [
    { text: "abc" },
    { text: "def" }
  ]
})

const force_added_ab = JSON.stringify({
  text: "Object string property B",
  obj: {
    sub: {
      sub_arr: [
        "one", "two", "three"
      ]
    },
    arr: [ "a", "b", "c", "d" ]
  },
  arr: [
    { text: "abc" },
    { text: "def" }
  ]
})



export default class ObjectUtilTest {
  static run() {


  }

  static add() {
    test('Add objects', () => {
      const result = JSON.stringify(
        ObjectUtil.add(object_a, object_b)
      );
      Assert.equal(result, added_ab, 
        `Unexpected object addition result! Expected: ${added_ab}, got: ${result}`
      );
    });
  }

  static force_add() {
    return test('Force add objects', () => {
      const result = JSON.stringify(
        ObjectUtil.force_add(object_a, object_b)
      );
      Assert.equal(result, force_added_ab, 
        `Unexpected forceful object addition result! Expected: ${force_added_ab}, got: ${result}`
      );
    });
  }
}
