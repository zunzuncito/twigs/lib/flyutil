
import { Worker } from 'worker_threads'

export default class MainWorker  {
  constructor(url) {
    this.worker = new Worker(url);

    this.recvs = {};

    const _this = this;
    const msg_cb = (data) => {
      if (!data.channel) data = data.data;
      const channel_id = data.channel;
      if (_this.recvs[channel_id]) _this.recvs[channel_id](data.data);
    }

    if (typeof window === 'undefined' || typeof process === 'object') {
      this.worker.on("message", msg_cb);
    } else {
      this.worker.onmessage = msg_cb;
    }
  }

  send(channel_id, data) {
    this.worker.postMessage({
      channel: channel_id,
      data: data
    });
  }

  recv(channel_id, func) {
    if (!this.recvs) this.recvs = {};
    this.recvs[channel_id] = func;
  }
}
