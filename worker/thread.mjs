
import MessageQueue from "./msg-queue.mjs"

export default class ThreadWorker  {
  constructor(self) {
    const _this = this;

    this.recvs = {};

    this.self = self;

    const msg_cb = (data) => {
      if (!data.channel) data = data.data;
      const channel_id = data.channel;
      if (_this.recvs[channel_id]) _this.recvs[channel_id](data.data)
    };

    if (typeof window === 'undefined' || typeof process === 'object') {
      self.on("message", msg_cb);
    } else {
      self.onmessage = msg_cb;
    }
  }

  send(channel_id, data) {
    this.self.postMessage({
      channel: channel_id,
      data: data
    });
  }

  recv(channel_id, func) {
    this.recvs[channel_id] = func;
  }
}
