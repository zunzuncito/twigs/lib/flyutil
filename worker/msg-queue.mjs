
export default class MessageQueue {
  constructor() {
    this.msgs = [];
  }

  add(id, data) {
    this.msgs.push({
      id: id,
      data: data
    });
  }

  next() {
    return this.msgs.shift();
  }
}
