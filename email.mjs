


export default class EmailUtil {
  static validate(email) {
    var re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/gm;
    return re.test(String(email).toLowerCase());
  }
}
