
import fs from 'fs'
import path from 'path'

const file_types = {
  json: ['json'],
  yaml: ['yaml', 'yml']
}


export default class PathUtil {

  static resolve_by_type(name, type, cwd) {
    if (!cwd) cwd = process.cwd();
    let resolved = undefined;
    for (let ext of file_types[type]) {
      const fpath = path.resolve(cwd, `${name}.${ext}`);
      if (fs.existsSync(fpath)) {
        resolved = fpath;
        break;
      }
    }
    if (!resolved) resolved = path.resolve(cwd, `${name}.${file_types[type][0]}`);
    return resolved;
  }

}
