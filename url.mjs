

export default class URLUtil {
  static get_parameter(name, url) {
    if (!url && window && window.location && window.location.href) {
      url = window.location.href;
    }
    return (new URL(url)).searchParams.get(name);
  }

  static set_parameter(name, value, url) {
    let search = url;
    if (typeof url !== "string") {
      search = window.location.search;
    }
    const searchParams = new URLSearchParams(search);
    searchParams.set(name, value);
    if (typeof url !== "string") {
      window.location.search = searchParams.toString();
    } else {
      return searchParams.toString();
    }
  }

  static delete_parameter(name, url) {
    const url_obj = new URL(url);
    url_obj.searchParams.delete(name);
    return url_obj.href;
  }
}



