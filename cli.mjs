

import readline from 'readline'
import { Writable } from 'stream'

export default class CLIUtil {
  static InputReader() {
    let std_out = new Writable({
      write: function(chunk, encoding, callback) {
        if (!this.muted)
          process.stdout.write(chunk, encoding);
        callback();
      }
    });

    let reader = readline.createInterface({
      input: process.stdin,
      output: std_out,
      terminal: true
    });

    reader.questionSync = async function(msg, pwd) {
      return await new Promise(function(resolve) {
        std_out.muted = false;
        reader.question(msg, function(input) {
          resolve(input);
        });
        if (pwd) std_out.muted = true;
      });
    };
    return reader;
  }
}

