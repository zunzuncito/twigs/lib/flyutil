
const SEC = 1000;
const MIN = SEC * 60;
const HR = MIN * 60;
const DAY = HR * 24;

export default class TimeUtil {
  static format(ms, fmt) {
    let dd = 0, hh = 0, mm = 0, ss = 0;

    if (fmt.includes('dd')) {
      dd = Math.floor(ms / DAY);
      ms -= dd * DAY;
    }

    if (fmt.includes('hh')) {
      hh = Math.floor(ms / HR);
      ms -= hh * HR;
      hh = hh.toString();
      while (hh.length < 2) hh = '0'+hh;
    }
    
    if (fmt.includes('mm')) {
      mm = Math.floor(ms / MIN);
      ms -= mm * MIN;
      mm = mm.toString();
      while (mm.length < 2) mm = '0'+mm;
    }
    
    if (fmt.includes('ss')) {
      ss = Math.floor(ms / SEC);
      ms -= ss * SEC;
      ss = ss.toString();
      while (ss.length < 2) ss = '0'+ss;
    }

    ms = ms.toString();
    while (ms.length < 3) ms = '0'+ms;

    let date_str = '';
    for (let fseg of fmt) {
      switch (fseg) {
        case 'dd':
          date_str += dd;
          break;
        case 'hh':
          date_str += hh;
          break;
        case 'mm':
          date_str += mm;
          break;
        case 'ss':
          date_str += ss;
          break;
        case 'ms':
          date_str += ms;
          break;
        default:
          date_str += fseg;
      }
    }

    return date_str;
  }

  getUTC() {
    return `UTC${(new Date().getTimezoneOffset() <= 0 ? "+" : "-")}${Math.abs(new Date().getTimezoneOffset()) / 60}`;
  }
}
