

import fs from "fs"
import path from "path"


export default class TwigsUtil {

  static get_layers_at_depth(depth, layers, twig_json, zunzun_cfg) {
    if (layers.length <= depth) layers.push([]);

    for (let type of ['lib', 'srv']) {
      for (let twig in twig_json[type]) {
        const dep = {
          type: type,
          name: twig,
          version: twig_json[type][twig],
          depth: depth
        }
        layers[depth].push(dep);

        const local_path = path.resolve(zunzun_cfg.local_twigs, type, twig);
        const lib_twig_json_path = path.resolve(local_path, 'twig.json');
        const lib_twig_json = JSON.parse(fs.readFileSync(lib_twig_json_path, 'utf8'));


        TwigsUtil.get_layers_at_depth(depth+1, layers, lib_twig_json, zunzun_cfg);
      }
    }
  }

  static get_layers(twig_json, zunzun_cfg) {
    let depth = 0;
    const layers = [[]];
    for (let type of ['lib', 'srv']) {
      for (let twig in twig_json[type]) {
        const dep = {
          type: type,
          name: twig,
          version: twig_json[type][twig],
          depth: depth
        }
        layers[depth].push(dep);

        const local_path = path.resolve(zunzun_cfg.local_twigs, type, twig);
        const lib_twig_json_path = path.resolve(local_path, 'twig.json');
        const lib_twig_json = JSON.parse(fs.readFileSync(lib_twig_json_path, 'utf8'));

        TwigsUtil.get_layers_at_depth(depth+1, layers, lib_twig_json, zunzun_cfg);

      }
    }

    let deepest = 0;
    const indexed_layers = [];
    for (let layer of layers) {
      for (let twig of layer) {
        let indexed = undefined;
        for (let itwig of indexed_layers) {
            if (twig.type == itwig.type && twig.name == itwig.name) {
              indexed = itwig;
            }
        }

        if (indexed) {
          indexed.depth = layers.indexOf(layer);
          if (deepest < indexed.depth) deepest = indexed.depth;
        } else {
          twig.depth = layers.indexOf(layer);
          if (deepest < twig.depth) deepest = twig.depth;
          indexed_layers.push(twig);
        }

      }
    }

    const filtered_layers = [];
    for (let i = 0; i < deepest+1; i++) {
      filtered_layers.push([]);
    }

    for (let itwig of indexed_layers) {
      filtered_layers[itwig.depth].push(itwig);
    }


    return filtered_layers;

  }

  
}
