
import { exec, spawn, execSync } from 'child_process'

export default class Exec {
  static command(cmd, cwd, stdio_inherit) {
    try {
      if (!cwd) cwd = process.cwd()
      console.debug(`\x1b[1m\x1b[40m\x1b[37m\x1b[4m > Run: \x1b[0m\x1b[1m\x1b[37m ${cmd}\x1b[1m \x1b[40m\x1b[37m\x1b[4m < CWD: \x1b[0m\x1b[1m\x1b[37m ${cwd}\x1b[0m`);
      const result = execSync(cmd, {
        cwd: cwd,
        stdio: stdio_inherit ? 'inherit' : 'pipe'
      });
      if (!stdio_inherit) {
        return result.toString();
      } else {
        return undefined;
      }
    } catch (e) {
      throw e;
    }
  }

  static async command_async(cmd, cwd, stdio_inherit) {
    try {
      if (!cwd) cwd = process.cwd()
      console.debug(`\x1b[1m\x1b[40m\x1b[37m\x1b[4m > Run: \x1b[0m\x1b[1m\x1b[37m ${cmd}\x1b[1m \x1b[40m\x1b[37m\x1b[4m < CWD: \x1b[0m\x1b[1m\x1b[37m ${cwd}\x1b[0m`);
      const result = await new Promise((resolve) => {
        const child = spawn(cmd, {
          cwd: cwd,
          stdio: stdio_inherit ? 'inherit' : 'pipe',
          shell: true
        });
        child.on('exit', (code, signal) => {
          resolve(code || signal);
        });
      });
      if (!stdio_inherit) {
        return result;
      } else {
        return undefined;
      }
    } catch (e) {
      throw e;
    }
  }

  static spawn(cmd, cwd, stdio_inherit) {
    try {
      if (!cwd) cwd = process.cwd()
      console.debug(`\x1b[1m\x1b[40m\x1b[37m\x1b[4m > Run: \x1b[0m\x1b[1m\x1b[37m ${cmd}\x1b[1m \x1b[40m\x1b[37m\x1b[4m < CWD: \x1b[0m\x1b[1m\x1b[37m ${cwd}\x1b[0m`);
      console.debug(``);
      const child = spawn(cmd, { // TODO REDUCE BUFFER SIZE FOR STDIO
        cwd: cwd,
        stdio: stdio_inherit ? 'inherit' : 'pipe',
        shell: true
      });

      return child;

    } catch (e) {
      throw e;
    }
  }
}

