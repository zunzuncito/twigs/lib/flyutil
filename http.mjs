
import fs from 'fs'

export default class HTTPUtil {
  static async file_stream(req, res, cfg) {
    let start, end;

    const range = req.headers.range;
    if (range) {
      const bytes_prefix = "bytes=";
      if (range.startsWith(bytes_prefix)) {
        const bytes_range = range.substring(bytes_prefix.length);
        const parts = bytes_range.split("-");
        if (parts.length === 2) {
          const range_start = parts[0] && parts[0].trim();
          if (range_start && range_start.length > 0) {
            start = parseInt(range_start);
          }
          const range_end = parts[1] && parts[1].trim();
          if (range_end && range_end.length > 0) {
            end = parseInt(range_end);
          }
        }
      }
    }

    const options = {
      start: start,
      end: end
    };

    cfg.headers['Content-Type'] = cfg.mime;

    const stat = fs.statSync(cfg.path);
    let content_length = stat.size;

    if (req.method === "HEAD") {
      cfg.headers["Accept-Ranges"] = "bytes";
      cfg.headers["Content-Length"] = content_length;
      res.writeHead(200, cfg.headers);
      res.end();
    } else {       
      let retrieved_length;
      if (start !== undefined && end !== undefined) {
        retrieved_length = (end+1) - start;
      } else if (start !== undefined) {
        retrieved_length = content_length - start;
      } else if (end !== undefined) {
        retrieved_length = (end+1);
      } else {
        retrieved_length = content_length;
      }

      const status_code = start !== undefined || end !== undefined ? 206 : 200;

      cfg.headers["Content-Length"] = retrieved_length;

      if (range !== undefined) {  
        cfg.headers["Content-Range"] = `bytes ${start || 0}-${end || (content_length-1)}/${content_length}`;
        cfg.headers["Accept-Ranges"] = "bytes";
      }

      res.writeHead(status_code, cfg.headers);
      const file_stream = fs.createReadStream(cfg.path, options);
      file_stream.pipe(res);
    }

  }

  static async multipart(req, path_cb) {
    try {
      return await new Promise((resolve) => {
        const content_type = req.headers['content-type'];
        const content_type_array = content_type.split(';').map(item => item.trim())
        const boundary_prefix = 'boundary='
        let boundary = content_type_array.find(item => item.startsWith(boundary_prefix))
        if (!boundary) return null
        boundary = boundary.slice(boundary_prefix.length)
        if (boundary) boundary = boundary.trim()

        const files_uploaded = [];

        let current_stream = undefined;
        req.on('data', (chunk) => {
          const boundaries = []

          let cloned_chunk = Buffer.from(chunk);
          while (cloned_chunk.indexOf(boundary) >= 0)  {
            const header_start = cloned_chunk.indexOf(boundary)-2;
            const header_end = cloned_chunk.indexOf("\r\n\r\n")+4;

            if (header_start < header_end) {
              const original_start = boundaries.length > 0 ?
                boundaries[boundaries.length - 1].header_end : 0;

              const start = original_start + header_start;
              const end = original_start + header_end;

              if (boundaries.length > 0) {
                boundaries[boundaries.length - 1].file_end = start;
              } else if (header_start != 0) {
                boundaries.push({
                  header_end: 0,
                  file_end: chunk.length
                });
              }
              
              const header_metadata = chunk.slice(start, end).toString();

              let filename = header_metadata.match(/(?:filename=")(.*?)(?:")/);
              if (!filename || filename.length < 2) {
                filename = undefined;
              } else {
                filename = filename[1]
              }

              const file_path = filename ? path_cb(filename) : undefined;

              if (filename) {
                boundaries.push({
                  header_end: end,
                  path: file_path
                });
              }

              cloned_chunk = cloned_chunk.slice(header_end);
            } else {
              cloned_chunk = Buffer.alloc(0);
            }
          }

          if (boundaries.length > 0) {
            for (let bound of boundaries) {
              console.log(bound);
              if (!bound.path) {
                const file_chunk = chunk.slice(bound.header_end, bound.file_end);
                current_stream.write(file_chunk);
              } else {
                if (current_stream) current_stream.end();
                current_stream = fs.createWriteStream(bound.path);
                files_uploaded.push(bound.path);
                const file_chunk = chunk.slice(bound.header_end, bound.file_end);
                current_stream.write(file_chunk);
              }
            }
          } else {
            current_stream.write(chunk);
          }
        });

        req.on('end', async () => {
          current_stream.end();
          resolve(files_uploaded);
        });
      });
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  static get_parameter(name, url) {
    return (new URL(url)).searchParams.get(name);
  }

  static set_parameter(name, value) {
    const searchParams = new URLSearchParams(window.location.search);
    searchParams.set(name, value);
    window.location.search = searchParams.toString();
  }
}


