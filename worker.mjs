
import MainWorker from './worker/main.mjs'
import ThreadWorker from './worker/thread.mjs'

export default class WorkerUtil {
  static Main = MainWorker
  static Thread = ThreadWorker
}
