
import ArrayUtil from './array.mjs'

export default class ObjectUtil {
  static add(a, b) {
    a = JSON.parse(JSON.stringify(a));
    for (let bkey in b) {
      if (a[bkey]) {
        if (Array.isArray(a[bkey]) && Array.isArray(b[bkey])) {
          a[bkey] = ArrayUtil.add(a[bkey], b[bkey]);
        } else if (
          typeof a[bkey] === 'object' &&
          typeof b[bkey] === 'object' &&
          a[bkey] !== null &&
          b[bkey] !== null
        ) {
          a[bkey] = ObjectUtil.add(a[bkey], b[bkey]);
        }
      } else {
        a[bkey] = b[bkey];
      }
    }
    return a;
  }

  static force_add(a, b) {
    a = JSON.parse(JSON.stringify(a));
    for (let bkey in b) {
      if (a[bkey]) {
        if (Array.isArray(a[bkey]) && Array.isArray(b[bkey])) {
          a[bkey] = ArrayUtil.add(a[bkey], b[bkey]);
        } else if (
          typeof a[bkey] === 'object' &&
          typeof b[bkey] === 'object' &&
          a[bkey] !== null &&
          b[bkey] !== null
        ) {
          a[bkey] = ObjectUtil.force_add(a[bkey], b[bkey]);
        } else {
          a[bkey] = b[bkey];
        }
      } else {
        a[bkey] = b[bkey];
      }
    }
    return a;
  }
}


