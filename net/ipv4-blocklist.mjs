
export default class IPv4Blocklist {
  static parse(bl_lines) {
    bl_lines = bl_lines.split("\n");
    const ips = [];
    for (let line of bl_lines) {
      let match = undefined;
      if ( // normal listing
        (match = line.match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/)) &&
        match.length > 0
      ) { 
        ips.push(match[0]);
      }
    }
    return ips;
  }

}
