
import path from "path";
import fs from "fs";

export default class FSUtil {

  static resolve_existing(dir_path, possible_names) {
    const existing = [];
    for (let fname of possible_names) {
      const fpath = path.resolve(dir_path, fname);
      if (fs.existsSync(fpath)) {
        existing.push(fpath);
      }
    }
    return existing;
  }

  static rm(file_path) {
    fs.unlinkSync(file_path);
  }

  static rmrf(dir_path) {
    if (fs.existsSync(dir_path)) fs.rmSync(dir_path, { recursive: true });
  }

  static mkdir(dir_path) {
    fs.mkdirSync(dir_path);
  }

  static force_mkdir(dir_path) {
    fs.mkdirSync(dir_path, { recursive: true });
  }

  static copy_dir(from, to) {
    FSUtil.force_mkdir(to);
    const dir_files = fs.readdirSync(from);
    for (const file of dir_files) {
      let file_path = path.resolve(from, file);
      const file_path_dest = path.resolve(to, file);
      let lstat = fs.lstatSync(file_path);
      if (lstat.isDirectory()) {
        FSUtil.copy_dir(file_path, file_path_dest);
      } else if (lstat.isSymbolicLink()) {
        file_path = fs.readlinkSync(file_path);
        fs.symlinkSync(file_path, file_path_dest);
      } else {
        fs.copyFileSync(file_path, file_path_dest)
      }
    }
  }

  static async read_dir(dir_path, file_cb, dir_cb) {
    try {
      if (!dir_cb) {
        dir_cb = () => {
          return true;
        }
      }

      const dir_files = fs.readdirSync(dir_path);
      for (let dir_file of dir_files) {
        const dir_file_path = path.resolve(dir_path, dir_file);
        if (fs.lstatSync(dir_file_path).isDirectory()) {
          if (await dir_cb(dir_file_path)) {
            await FSUtil.read_dir(dir_file_path, file_cb, dir_cb);
          }
        } else {
          let link_to_dir = false;

          const lstats = fs.lstatSync(dir_file_path);
          if (lstats.isSymbolicLink()) {
            const ltarget = path.resolve(dir_path, fs.readlinkSync(dir_file_path));
            if (fs.existsSync(ltarget) && fs.lstatSync(ltarget).isDirectory()) link_to_dir = true;
          }

          if (link_to_dir) {
            if (await dir_cb(dir_file_path)) {
              await FSUtil.read_dir(dir_file_path, file_cb, dir_cb);
            }
          } else {
            await file_cb(dir_path, dir_file);
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  static read_dir_async(dir_path, file_cb, dir_cb) {
    if (!dir_cb) {
      dir_cb = () => {
        return true;
      }
    }

    const dir_files = fs.readdirSync(dir_path);
    for (let dir_file of dir_files) {
      const dir_file_path = path.resolve(dir_path, dir_file);
      if (fs.lstatSync(dir_file_path).isDirectory()) {
        if (dir_cb(dir_file_path)) {
          FSUtil.read_dir_async(dir_file_path, file_cb, dir_cb);
        }
      } else {
        let link_to_dir = false;

        const lstats = fs.lstatSync(dir_file_path);
        if (lstats.isSymbolicLink()) {
          const ltarget = path.resolve(dir_path, fs.readlinkSync(dir_file_path));
          if (fs.existsSync(ltarget) && fs.lstatSync(ltarget).isDirectory()) link_to_dir = true;
        }

        if (link_to_dir) {
          if (dir_cb(dir_file_path)) {
            FSUtil.read_dir_async(dir_file_path, file_cb, dir_cb);
          }
        } else {
          file_cb(dir_path, dir_file);
        }
      }
    }
  }

  static symlink(from, to) {
    if (fs.existsSync(to)) fs.unlinkSync(to);
    if (fs.existsSync(from)) {
      fs.symlinkSync(from, to);
    } else {
      throw new Error(`Failed to create symbolic link from ${from} to ${to}! Source file does not exist!`);
    }
  }

  static watch_sync_dirs_at_depth(src, dst, files_to_watch) {
    let update_queue = [];

    fs.watch(src, (evt_type, filename) => {
      const fullpath = path.resolve(src, filename);
      if (files_to_watch.includes(filename)) {
        if (!update_queue.includes(filename) && fs.existsSync(fullpath)) {
          update_queue.push(filename);
        }
      } else if (fs.existsSync(fullpath)) {
        files_to_watch.push(filename);
      }
    });

    setInterval(() => {
      for (let filename of update_queue) {
        const src_path = path.resolve(src, filename);
        const dst_path = path.resolve(dst, filename);
        console.log(`[CHANGE] >> SYNC
        src: ${src_path}
        dst: ${dst_path}`);
        if (fs.existsSync(src_path)) {
          fs.copyFileSync(src_path, dst_path);
        }
      }
      update_queue = [];
    }, 100);
  }

  static sync_dirs_at_depth(src, dst, ign) {
    if (!ign) ign = [];
    const files_to_watch = [];

    if (!fs.existsSync(dst)) fs.mkdirSync(dst);
    const dir_files = fs.readdirSync(src);
    for (const file of dir_files) {
      if (!ign.includes(file)) {
        const file_path = path.resolve(src, file);
        const file_path_dest = path.resolve(dst, file);
        if (fs.lstatSync(file_path).isDirectory()) {
          FSUtil.sync_dirs_at_depth(file_path, file_path_dest);
        } else {
          try {
            fs.copyFileSync(file_path, file_path_dest)
          } catch (e) {}
        }
      }
    }

  }

  static sync_dirs(src, dst, ign) {
    console.debug(`Sync directories:
      src: ${src}
      dst: ${dst}`);
    FSUtil.sync_dirs_at_depth(src, dst, ign);
  }

  static sync_dirs_watch(src, dst, ign) {
    console.debug(`Sync and watch directories:
      src: ${src}
      dst: ${dst}`);

    if (!ign) ign = [];

    const files_to_watch = [];

    if (!fs.existsSync(dst)) fs.mkdirSync(dst);

    const dir_files = fs.readdirSync(src);
    for (const file of dir_files) {
      if (!ign.includes(file)) {
        const file_path = path.resolve(src, file);
        const file_path_dest = path.resolve(dst, file);
        if (fs.lstatSync(file_path).isDirectory()) {
          FSUtil.sync_dirs_at_depth(file_path, file_path_dest, ign);
          FSUtil.sync_dirs_watch(file_path, file_path_dest, ign)
        } else {
          try {
            fs.copyFileSync(file_path, file_path_dest)
            files_to_watch.push(file);
          } catch (e) {}
        }
      }
    }

    FSUtil.watch_sync_dirs_at_depth(src, dst, files_to_watch, ign);
  }

  static move(src, dst) {
    const src_files = fs.readdirSync(src);
    for (let file of src_files) {
      const src_path = path.join(src, file);
      const dst_path = path.join(dst, file);
      fs.renameSync(src_path, dst_path);
    }
  }

}
