# Zunzuncito Utilities

Official zunzun utility library


## ArrayUtil
```
import ArrayUtil from 'zunzun/flyutil/array.mjs'
```

### add(a, b)
add `b` array on `a`


## CLIUtil
```
import CLIUtil from 'zunzun/flyutil/cli.mjs'
```

### InputReader()
sets up and returns a `stdin` reader interface.


## EmailUtil
```
import EmailUtil from 'zunzun/flyutil/email.mjs'
```

### validate(email)


## ExecUtil
```
import ExecUtil from 'zunzun/flyutil/exec.mjs'
```

### command(cmd, cwd, stdio_inherit)


## FSUtil
```
import ExecUtil from 'zunzun/flyutil/fs.mjs'
```

### rm(file_path)

### rmrf(dir_path)

### copy_dir(from, to)

### async read_dir_async(dirpath, file_cb, dir_cb)

### read_dir(dir_path, file_cb, dir_cb)

### symlink(from, to)

### watch_sync_dirs_at_depth(src, dst, files_to_watch)

### sync_dirs_at_depth(src, dst)

### sync_dirs(src, dst)


## ImportUtil
```
import ImportUtil from 'zunzun/flyutil/import.mjs'
```

### async import(module_dir, module_name)

## LogUtil
```
import LogUtil from 'zunzun/flyutil/log.mjs'
```
empty for now


## ObjectUtil
```
import ObjectUtil from 'zunzun/flyutil/object.mjs'
```

### add(a, b)
add `b` object on `a` without overwriting

### force_add(a, b)
add `b` object on `a` forcefuly overwriting


## PathUtil
```
import PathUtil from 'zunzun/flyutil/path.mjs'
```

### resolve_by_type(name, type, cwd)
resolve files with extension variants


## URLUtil
```
import URLUtil from 'zunzun/flyutil/url.mjs'
```

### get_parameter(name, url)

### set_parameter(name, value)



