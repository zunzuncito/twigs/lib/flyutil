
export default class ArrayUtil {
  static add(a, b) {
    for (const bai of b) {
      let duplicate = false;
      for (const aai of a) {
        if (aai === bai)  {
          duplicate = true;
          break;
        } else if (
          typeof aai === 'object' &&
          typeof bai === 'object' &&
          JSON.stringify(aai) === JSON.stringify(bai)
        ) {
          duplicate = true;
          break;
        }
      }
      if (!duplicate) a.push(bai);
    }
    return a;
  }
}
