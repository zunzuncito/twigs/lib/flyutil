
const ANSI_ESCAPE_CODES = [
  0, // reset
  1, // bright
  2, // dim
  4, // underscore
  5, // blink
  7, // reverse
  8, // hidden

  30, // fg black
  31, // fg red
  32, // fg green
  33, // fg yellow
  34, // fg blue
  35, // fg magneta
  36, // fg cyan
  37, // fg white
  90, // fg gray

  40, // bg black
  41, // bg red
  42, // bg green
  43, // bg yellow
  44, // bg blue
  45, // bg magneta
  46, // bg cyan
  47, // bg white
  100 // bg gray
]

export default class LogUtil {
  static msg(...msgs) {
    let log_string = '';
    for (let msg of msgs) {
      if (log_string.length > 0) log_string += ' ';
      if (typeof msg === "string") {
        log_string += msg;
      } else {
        for (let val of msg) {

          if (Number.isInteger(val)) {
            log_string += `\x1b[${val}m`;
          } else {
            log_string += val;
          }
        }
      }
    }
    console.log(log_string);
  }

  static err() {

  }

  static dbg() {

  }

  static nl() {
    console.log("");
  }
}
